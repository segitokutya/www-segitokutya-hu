+++
title = "Linkek"
subtitle = "Hasznos linkek gyűjteménye."
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-1.jpg"
tags = ["linkek"]
categories = ["info"]
summary = "Hasznos linkek gyűjteménye."
icon = "fas fa-paper-plane"
type = "iconized"
+++

Magyar Terápiás és Segítőkutyás Szövetség Egyesület http://www.matesze.hu/web/vendeg/magunkrol
Magyar Terápiás és Segítőkutyás Szövetség Egyesület
http://www.matesze.hu/web/vendeg/magunkrol

Kutyával az Emberért Alapítvány http://www.kea-net.hu/
Kutyával az Emberért Alapítvány
http://www.kea-net.hu/

NEO Magyar Segítőkutya Egyesület http://segitokutya.net/
NEO Magyar Segítőkutya Egyesület
http://segitokutya.net/

- Négylábúak az Emberért Közhasznú Alapítvány http://neka.atw.hu/
– Négylábúak az Emberért Közhasznú Alapítvány
http://neka.atw.hu/

Csiga-Biga Alapítvány http://www.csigabigaalapitvany.hu/
Csiga-Biga Alapítvány
http://www.csigabigaalapitvany.hu/

Misina Természet- és Állatvédő egyesület http://www.misina.hu/
Misina Természet- és Állatvédő egyesület
http://www.misina.hu/


Autista Gyermekekért Egyesület http://www.agye.hu/?menu=7&sub=0
Autista Gyermekekért Egyesület
http://www.agye.hu/?menu=7&sub=0

Kecskeméti Autizmus Centrum http://www.gyogyped.hu/node/207
Kecskeméti Autizmus Centrum
http://www.gyogyped.hu/node/207

Halmozottan Sérültek és Szüleiknek Szabolcs-Szatmár-Bereg Megyei Egyesülete http://hse.fw.hu/index.php?id=11
Halmozottan Sérültek és Szüleiknek Szabolcs-Szatmár-Bereg Megyei Egyesülete
http://hse.fw.hu/index.php?id=11

Országos Foglakoztatási Közalapítvány http://www.ofa.hu/
Országos Foglakoztatási Közalapítvány
http://www.ofa.hu

Dr. Gargya Sándor állatorvos Debrecen-józsa, Józsavet http://jozsavet.lapunk.hu/
Dr. Gargya Sándor állatorvos Debrecen-józsa, Józsavet
http://jozsavet.lapunk.hu/

Köböl Erika - Kutyás-terápia http://www.kutyas-terapia.hu/kutyas-terapia/index.php
Köböl Erika – Kutyás-terápia
http://www.kutyas-terapia.hu/kutyas-terapia/index.php

Pető Mihályné Donát Dóra - Bíbor & Bársony a terápiás kutyák világában http://www.facebook.com/pages/B%C3%8Dbor-B%C3%A1rsony-a-ter%C3%A1pi%C3%A1s-kuty%C3%A1k-vil%C3%A1g%C3%A1ban/300421839975810
Pető Mihályné Donát Dóra – Bíbor & Bársony a terápiás kutyák világában
http://www.facebook.com/pages/B%C3%8Dbor-B%C3%A1rsony-a-ter%C3%A1pi%C3%A1s-kuty%C3%A1k-vil%C3%A1g%C3%A1ban/300421839975810


Fogyatékos Személyek Esélyegyenlőségéért http://fszk.hu/
Fogyatékos Személyek Esélyegyenlőségéért
http://fszk.hu/



KajásRobi a Kutyás Gyerektábort melegétellel támogatta, köszönjük http://kajasrobi.hu/
KajásRobi a Kutyás Gyerektábort melegétellel támogatta, köszönjük
http://kajasrobi.hu/
