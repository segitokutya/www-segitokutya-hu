+++
title = "Pályázatok"
subtitle = "Folyamatban lévő pályázatok."
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-3.jpg"
tags = ["pályázatok"]
categories = ["info"]
summary = "Folyamatban lévő pályázatok."
icon = "fas fa-envelope"
type = "iconized"
+++

## EFOP Pályázatok

A következő EFOP pályázatok futnak az AURA alapítvánnyal.

### Közösségfejlesztés, társadalmi szerepvállalás erősítése segítő kutyákkal

Az Emberi Erőforrások Minisztériuma EU Fejlesztések Végrehajtásáért Felelős Helyettes Államtitkárság, mint Támogató az Emberi Erőforrás Fejlesztési Operatív Program (EFOP) keretén belül EFOP-1.3.5-16 Társadalmi szerepvállalás erősítése a közösségek fejlesztésével tárgyú felhívást tett közzé, melyre EFOP-1.3.5-16-2016-00-597 azonosító számon támogatási kérelmet nyújtottunk be. A Támogató döntése alapján az AURA Segítő Kutya Alapítvány támogatásban részesült.

A projekt címe: “Közösségfejlesztés, társadalmi szerepvállalás erősítése segítő kutyákkal.”

Projekt megvalósítási időszakának kezdő időpontja: 2017. 04. 10.

A projekt befejezésének tervezett napja: 2020. 04. 10.

{{< figure src="/img/efop_1.3.5-16_palyazat.jpg" alt="EFOP" width="80%" >}}
