+++
title = "Nyílvános adatok"
subtitle = "Az alapítvánnyal kapcsolatos nyílvános adatok."
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-2.jpg"
tags = ["alapítvány"]
categories = ["info"]
summary = "Az alapítvánnyal kapcsolatos nyílvános adatok."
icon = "fas fa-heartbeat"
type = "iconized"
+++

## AURA Segítő Kutya Alapítvány

Levelezési cím: 4031 Debrecen, Határ út 5.

Adószám: `18995736-1-09`

Bankszámlaszám: Raiffeisen Bank: `12052712-01529583-00100000`

## Közhasznúsági jelentések

2017.
