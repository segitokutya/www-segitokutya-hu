+++
title = "Rendeletek"
subtitle = "A segítő- és terápiáskutyák jogszabályozása."
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-4.jpg"
tags = ["rendelet", "jogszabály"]
categories = ["info"]
summary = "A segítő- és terápiáskutyák jogszabályozása."
icon = "fas fa-sliders-h"
type = "iconized"
[blackfriday]
  smartypants = false
+++

## 27/2009. (XII. 3.) SZMM rendelet

_**A rendelet mindenkori aktuális verziója [itt található](https://net.jogtar.hu/jogszabaly?docid=A0900027.SMM).
A lenti szöveg csak a tájékoztatás célját szolgálja, felelősséget annak esetleges hibáiért nem vállalunk.**_

27/2009. (XII. 3.) SZMM rendelet
a segítő kutya kiképzésének, vizsgáztatásának és alkalmazhatóságának szabályairól
A fogyatékos személyek jogairól és esélyegyenlőségük biztosításáról szóló 1998. évi XXVI. törvény 30. § (2) bekezdésében kapott felhatalmazás alapján, a szociális és munkaügyi miniszter feladat- és hatásköréről szóló 170/2006. (VII. 28.) Korm. rendelet 1. § h) pontjában megállapított feladatkörömben eljárva a következőket rendelem el:

#### 1. § E rendelet alkalmazásában

* (a) fogyatékossággal élő személy: a fogyatékos személyek jogairól és esélyegyenlőségük biztosításáról szóló 1998. évi XXVI. törvény (a továbbiakban: Fot.) 4. § a) pontjában és a Fogyatékossággal élő személyek jogairól szóló egyezmény és az ahhoz kapcsolódó Fakultatív Jegyzőkönyv kihirdetéséről szóló 2007. évi XCII. törvény 1. cikke szerinti személy,

* b) *  segítő kutya: a fogyatékossággal élő személyt az egyenlő esélyű hozzáféréshez fűződő joga gyakorlásában, önálló életvitelének elősegítésében, illetve veszélyhelyzet elhárításában segítő, habilitációs, rehabilitációs feladatokat ellátó, a külön jogszabályban meghatározott állat-egészségügyi követelményeknek megfelelő kutya,

* c) gazda:

* * ca) az a fogyatékossággal élő személy, aki segítő kutyát alkalmaz és a segítő kutya használatára, gondozására vonatkozó képzésben részt vett, valamint eredményes vizsgát tett,

* * cb) a terápiás kutyát irányító személy, aki a segítő kutya használatára, gondozására vonatkozó képzésben részt vett és eredményes vizsgát tett,

* d) közterület: az épített környezet alakításáról és védelméről szóló 1997. évi LXXVIII. törvény 2. § 13. pontjában meghatározott fogalom,

* e) bevásárlóközpont: a kereskedelemről szóló 2005. évi CLXIV. törvény (a továbbiakban: Kertv.) 2. § 3. pontjában meghatározott fogalom,

* f) piac: a Kertv. 2. § 19. pontjában meghatározott fogalom,

* g) szálláshely: a Kertv. 2. § 22. pontjában meghatározott fogalom,

* h) üzlet: a Kertv. 2. § 27. pontjában meghatározott fogalom,

* i) vásár: a Kertv. 2. § 29. pontjában meghatározott fogalom,

* j) állatkert: az állatok védelméről és kíméletéről szóló 1998. évi XXVIII. törvény (a továbbiakban: Ávt.) 3. § 3. pontjában meghatározott létesítmény.

#### 2. § (1) A segítő kutya típusa kiképzése és alkalmazhatósága szerint

* a) vakvezető kutya: a látássérült személy vezetésére kiképzett kutya,

* b) mozgáskorlátozott személyt segítő kutya: a mozgáskorlátozott személyt mindennapi tevékenységeinek ellátásában segítő feladatokra kiképzett kutya,

* c) hangot jelző kutya: a hallássérült személy számára veszélyt vagy egyéb fontos információt jelentő hangok jelzésére kiképzett kutya,

* d) rohamjelző kutya: az epilepsziával élő személy vagy más krónikus, rohamszerű állapotoktól veszélyeztetett személy számára a roham során segítséget nyújtó feladatokra kiképzett kutya,

* e) személyi segítő kutya: a fogyatékos személyt önálló életvitelében segítő feladatokra kiképzett kutya,

* f) terápiás kutya: a gyógypedagógiai, a szociális szolgáltatások területén pedagógiai, pszichológiai, pszichiátriai, konduktív pedagógiai habilitációs, illetve rehabilitációs folyamatban alkalmazott kutya

lehet.

(2) A segítő kutyát megkülönböztető jelzéssel kell ellátni, amelyen a 3. § (1) bekezdés szerinti kutyát kiképző szervezet logójának szerepelnie kell.

(3) Ha a terápiás kutyát nem a kutyát kiképző szervezet alkalmazza, akkor a kutyának a kutyát alkalmazó szervezet logóját tartalmazó megkülönböztető jelzést kell viselnie.

#### 3. § (1) Az a szervezet (a továbbiakban: kutyát kiképző szervezet) képezhet ki segítő kutyát, és készíthet fel a segítő kutya használatára, gondozására,

* a) amelynek létesítő okiratában a segítő kutyák kiképzése és a fogyatékossággal élő személyekhez történő eljuttatása feltüntetésre került, és

* b) amely biztosítja, hogy a kiképzést olyan személy (a továbbiakban: kiképző) végzi, aki

* * ba) terápiás kutya kiképzése esetén az Országos Képzési Jegyzékben szereplő habilitációs kutya kiképzője (OKJ 52 810 01) vagy habilitációs kutyakiképző (OKJ 52 810 01 10 00 00 00) szakképesítéssel rendelkezik,

* * bb) rohamjelző és hangot jelző kutya kiképzése esetén az Országos Képzési Jegyzékben szereplő jelző kutya kiképzője (OKJ 53 810 01) vagy jelzőkutya-kiképző (OKJ 52 810 01 00 01 52 02) szakképesítéssel rendelkezik,

* * bc) mozgáskorlátozott személyt segítő és személyi segítő kutya kiképzése esetén az Országos Képzési Jegyzékben szereplő mozgássérültet segítő kutya kiképzője (OKJ 53 810 02) vagy mozgássérültet segítő kutya kiképző (OKJ 52 810 01 00 01 52 01) szakképesítéssel rendelkezik,

* * bd) vakvezető kutya kiképzése esetén az Országos Képzési Jegyzékben szereplő vakvezető kutya kiképzője (OKJ 53 810 03) vagy vakvezetőkutya kiképző (OKJ 52 810 01 00 01 52 03) szakképesítéssel rendelkezik, továbbá

* c) amely vakvezető kutya kiképzése esetén biztosítja, hogy a látássérült emberek kutyával közlekedésére való felkészítését a szakképzett kutyakiképző mellett gyógypedagógus látássérültek pedagógiája szakirányon szakképzettséggel, illetve rehabilitációs szakember a látássérülés területén szakképzettséggel rendelkező személy végzi.

(2) Terápiás kutyát alkalmazó szervezet az a kutyát kiképző szervezet, amely az általa vagy egy másik kutyát kiképző szervezet által kiképzett és levizsgáztatott terápiás kutyát és gazdáját alkalmazza.

#### 4. § (1) A szociál- és nyugdíjpolitikáért felelős miniszter pályázat útján a kutyát kiképző szervezetek, illetve a segítő kutyák alkalmazásának elősegítésével foglalkozó szervezetek közül közreműködő szervezetet jelöl ki, amely

* a) honlapján közzéteszi

* * aa) tájékoztató jelleggel a kutyát kiképző szervezetek listáját,

* * ab) az 5. § (4) bekezdés szerinti követelményeket,

* b) szakmai tanácsadással segíti a kutyát kiképző szervezetek munkáját,

* c) biztosítja az 5. § (1) bekezdése szerinti vizsga jogszabályban és a szakmai szabályokban foglaltak szerinti lefolytatását és kiállítja az eredményes vizsga letételéről szóló tanúsítványt,

* d) a kutyát kiképző szervezetek kiképzői számára fényképes segítőkutya-kiképző tanúsítványt állít ki.

(2) Az (1) bekezdés szerinti tevékenységéért a közreműködő szervezet a vizsga szervezésével kapcsolatban ténylegesen felmerült adminisztrációs költségeinek (a vizsgát szervező adminisztrátor munkadíja, telefon- és postaköltség, a vizsgadokumentumok előállításának költségei, tanúsítvány kiállításának költsége) fedezésére a kiképző szervezettől díjat kérhet, amely vizsgára bejelentett kutyánként nem haladhatja meg az öregségi nyugdíj mindenkori legkisebb összegének 20%-át.

(3) A közreműködő szervezet kijelölése öt évre szól. Ugyanazon szervezet több alkalommal is kijelölhető közreműködő szervezetnek.

#### 5. § (1) A segítő kutya kiképzése és a fogyatékossággal élő személynek a segítő kutya használatára, gondozására való felkészítése a mellékletben meghatározott vizsgarend szerint, vizsgabizottság előtt lefolytatott vizsgával zárul.

(2) A vizsga megszervezéséről, a vizsga lebonyolításához szükséges személyi és tárgyi feltételek meglétéről a kutyát kiképző szervezet gondoskodik.

(3) A kutya gazdája által fizetendő vizsgadíj összegét a kutyát kiképző szervezet határozza meg azzal, hogy annak összege nem haladhatja meg az öregségi nyugdíj mindenkori legkisebb összegének 20%-át.

(4) A gazdának és a segítő kutyának a vizsgán a közreműködő szervezet által kidolgozott és a szociál- és nyugdíjpolitikáért felelős miniszter által jóváhagyott szabályzatban meghatározott követelményeknek kell megfelelni.

#### 6. § (1) A vizsgabizottság

* a) a közreműködő szervezet által delegált két tagból,

* b) a kutyát kiképző szervezet által delegált tagból,

* c) - ha a fogyatékossággal élő személy vagy törvényes képviselője kéri - az általuk megjelölt, a fogyatékossággal élő személy fogyatékosságának vagy betegségének megfelelő szakterületen működő érdekvédelmi szervezetet által delegált tagból

áll.

(2) Az (1) bekezdés a) és b) pontja szerinti vizsgabizottsági tagnak az Országos Képzési Jegyzékben szereplő habilitációs kutyakiképző szakképesítéssel és legalább 5 éves, a segítő kutyák kiképzésében, illetve vizsgáztatásában szerzett szakmai gyakorlattal kell rendelkeznie.

(3) A közreműködő szervezet, illetve az érdekvédelmi szervezet által delegált tag vagy annak hozzátartozója nem lehet tagja, illetve alkalmazottja a kutyát kiképző szervezetnek.

(4) A kutyát kiképző szervezet által delegált tag nem lehet a vizsgáztatandó kutya kiképzője vagy nevelője.

(5) A közreműködő szervezet és az érdekvédelmi szervezet az általa delegált tagokról, illetve tagról a kutyát kiképző szervezet felkérésének átvételét követő 15 napon belül írásban értesíti a kutyát kiképző szervezetet. A közreműködő szervezet az értesítésben megjelöli, hogy az általa delegált tagok közül ki a vizsgabizottság elnöke.

#### 7. § (1) A kutyát kiképző szervezet a vizsgabizottsági tagok véleményének kikérésével megállapítja a vizsga időpontját és helyszínét, valamint a vizsgáztatás lefolytatása céljából összehívja a vizsgabizottságot.

(2) A vizsga helyszínét az 5. § (4) bekezdés szerinti szabályzatban foglaltak alapján kell meghatározni oly módon, hogy az megfeleltethető legyen a segítő kutya leendő alkalmazási körülményeinek.

(3) A vizsgabizottság tagjait a vizsgabizottság összehívásáról legkésőbb a vizsga időpontját megelőző 15 nappal írásban értesíteni kell. Ezzel egyidejűleg a közreműködő szervezet honlapján közzé kell tenni

* a) a vizsga helyszínét,

* b) a vizsga időpontját,

* c) hozzájárulásuk esetén a vizsgabizottság tagjainak nevét,

* d) a vizsgára bejelentett segítő kutya nevét,

* e) hozzájárulása esetén a segítő kutya kiképzőjének nevét, és

* f) hozzájárulása esetén a segítő kutya gazdájának nevét.

(4) A vizsga időpontjáról és helyszínéről, továbbá a vizsgabizottság tagjairól legkésőbb a vizsga időpontját megelőző 15 nappal írásban értesíteni kell a vizsgázó személyt és annak törvényes képviselőjét. Az értesítésben fel kell hívni a vizsgázó személy és a törvényes képviselő figyelmét a vizsga megkezdésének és zavartalan lefolytatásának feltételeire, valamint tolmács vagy jelnyelvi tolmács igénybevételének lehetőségére.

(5) Az eredményes vizsga letételéről a közreműködő szervezet tanúsítványt állít ki a gazda részére.

#### 8. § (1) A kutyát kiképző szervezet és a gazda a segítő kutya átadásáról írásban szerződést köt, amely tartalmazza

* a) az átadás jogcímét (pl. adásvétel, használatba adás),

* b) a kutyát kiképző szervezet nevét, székhelyét, elérhetőségét,

* c) a kutyát kiképző szervezet felelősségvállalását arra vonatkozóan, hogy a segítő kutya az átadáskor megfelel a külön jogszabályban foglalt állat-egészségügyi követelményeknek,

* d) a szerződésből a kutyát kiképző szervezetre és a gazdára háruló jogokat és kötelezettségeket, így különösen tételesen a gazdát terhelő valamennyi fizetési kötelezettséget,

* e) a segítő kutyának a kutyát kiképző szervezet részére történő visszaadásának lehetőségét, okait vagy a segítő kutya visszaadásának kizárását.

(2) A segítő kutya átadásakor a kutyát kiképző szervezet tájékoztatja a gazdát a szavatossági igény érvényesítésének határidejéről.

#### 9. § (1) A Fot. 7/A. § (1) bekezdésében meghatározott, a közszolgáltatásokhoz való egyenlő esélyű hozzáférés biztosítása érdekében a gazda és a kiképző (a továbbiakban együtt: segítő kutyát alkalmazó személy) jogosult a - (2) bekezdés szerinti kivétellel - segítő kutyával tartózkodni és a segítő kutyát használni a lakosság elől elzárt területek kivételével a Fot. 4. § f) pontja szerinti közszolgáltatást nyújtó szerv, intézmény, szolgáltató területén és egyéb, mindenki számára nyitva álló létesítményben (területen), így különösen

* a) közforgalmú tömegközlekedési eszközön,

* b) üzletben, ideértve az élelmiszert árusító üzletet és a vendéglátó üzletet is,

* c) bevásárlóközpontban,

* d) piacon,

* e) vásáron,

* f) szálláshelyen,

* g) játszótéren,

* h) közművelődési, oktatási, szociális, gyermekjóléti, gyermekvédelmi intézményben,

* i) közfürdő területén,

* j) állatkertben,

* k) közterületen.

(2) Terápiás kutyával tartózkodni és terápiás kutyát használni a lakosság elől elzárt területek kivételével az (1) bekezdés h) pontja szerinti intézményben lehet.

(2a) A 4. § (1) bekezdés d) pontja szerinti tanúsítvánnyal rendelkező személy jogosult arra, hogy a kiképzés alatt álló kutyával kiképzés céljából az (1) bekezdés szerinti létesítményben tartózkodjon.

(3) Mozgólépcsővel felszerelt létesítményben (területen) a segítő kutyát alkalmazó személy jogosult a segítő kutyával az üzemképes, álló mozgólépcsőt használni.

(4) A létesítmény (terület) fenntartója, üzemeltetője, tulajdonosa, illetve az általa megbízott személy a segítő kutyát akkor távolíthatja el a létesítményből (területről), ha a segítő kutya a létesítményben (területen) tartózkodók testi épségét veszélyeztető magatartást tanúsít.

#### 10. § (1) Az alkalmazásban álló és az alkalmazásból kivont segítő kutyára a helyi önkormányzat ebtartást szabályozó rendeletében a kutyák számára és méretére vonatkozó előírásokat nem kell alkalmazni.

(2) A segítő kutyát alkalmazó személy szájkosár nélkül alkalmazhatja a segítő kutyát.

(3) Az alkalmazásból kivont segítő kutya számára a 4. § (1) bekezdése szerinti közreműködő szervezet külön tanúsítványt állít ki, amelyen a kutya alkalmazásból való kivonásának ténye rögzítésre kerül.

#### 11. § A 9. és 10. §-t alkalmazni kell a Magyarországra már kiképzetten behozott segítő kutyára és gazdájára is, feltéve, hogy a segítő kutya és gazdája olyan egyértelmű azonosításra alkalmas okmánnyal rendelkezik, amelyet vakvezető kutyák esetén a Europoean Guide Dog Federation (EGDF) vagy az International Guide Dog Federation (IGDF), más segítő kutyák esetén az Assistance Dogs Europe (ADEu) vagy az Assistance Dogs International (ADI) tagszervezete állított ki.

#### 12. § (1) A segítő kutya tartására és védelmére az Ávt. rendelkezései az irányadóak azzal, hogy

* a) - a gazda és a kutyát kiképző szervezet eltérő megállapodása hiányában - a kutyát kiképző szervezet köteles gondoskodni az alkalmazásból kivont segítő kutyáról,

* b) az állat tartójának a segítő kutya kiképzésének ideje alatt a kiképző, a segítő kutya átadását követően a gazda minősül.

(2) A segítő kutya által okozott kárért való felelősségre a Polgári Törvénykönyvről szóló 2013. évi V. törvénynek az állattartók felelősségére vonatkozó szabályai az irányadóak azzal, hogy az állat tartójának a segítő kutya kiképzésének ideje alatt a kiképző, a segítő kutya átadását követően a gazda minősül.

(3) A segítő kutyával kapcsolatban elkövetett jogsértés miatt a gazda és a kutyát kiképző szervezet is jogosult fellépni.

#### 13. § (1) Ez a rendelet - a (2) bekezdésben meghatározott kivétellel - a kihirdetését követő nyolcadik napon lép hatályba.

(2) E rendelet 6. § (2) bekezdése 2017. január 1-jén lép hatályba.

(3) Az e rendelet hatálybalépésekor segítő kutyát kiképző szervezet tekintetében az e rendelet 3. § (1) bekezdés b) pontjában foglaltakat 2012. január 1-jétől kell alkalmazni.

(4) E rendelet 9. és 10. §-át az e rendelet hatálybalépését megelőzően segítő kutyává kiképzett kutya és gazdája tekintetében - az (5) bekezdésben foglalt kivétellel - az 5. § (1) bekezdés szerinti vizsga hiányában is alkalmazni kell.

(5) Az e rendelet hatálybalépését megelőzően terápiás kutyának kiképzett kutya terápiás kutyaként 2010. december 31-ét követően az 5. § (1) bekezdése szerinti eredményes vizsga megléte esetén alkalmazható.

#### 14. § E rendelet a belső piaci szolgáltatásokról szóló, 2006. december 12-i 2006/123/EK európai parlamenti és tanácsi irányelvnek való megfelelést szolgálja.

----

## Melléklet a 27/2009. (XII. 3.) SZMM rendelethez

A segítő kutya kiképzését és a gazdának a segítő kutya használatára, gondozására való felkészítését lezáró vizsga rendje

#### I. A vizsgabizottság tevékenysége

1. A vizsgabizottság a vizsga megkezdése előtt kezdő értekezlet keretében ellenőrzi a vizsgához szükséges tárgyi feltételek meglétét.

2. Egy vizsgabizottság előtt a vakvezető kutya és gazdája, a mozgáskorlátozott személyt segítő kutya és gazdája, a hangot jelző kutya és gazdája, a rohamjelző kutya és gazdája, a személyi segítő kutya és gazdája tekintetében meghatározott vizsgák, valamint a terápiás kutya és gazdája tekintetében meghatározott második részvizsga esetében egy napon összesen legfeljebb 6 fő, a terápiás kutya és gazdája tekintetében meghatározott első részvizsga esetében pedig összesen legfeljebb 10 fő vizsgázhat. Amennyiben a vizsgázó személyek száma meghaladja a fent meghatározott létszámot, több vizsgabizottság összeállítása szükséges.

3. A vizsgabizottság a vizsga során biztosítja a vizsga szabályos, zavartalan lefolytatását, szükség esetén megteszi a II/4-8. pont szerinti intézkedéseket.

4. A vizsgabizottság a vizsga lefolytatását követően záróértekezlet keretében értékeli a vizsgázó személyek és a vizsgára bocsátott segítő kutyák teljesítményét.

5. A vizsgabizottság tagjait útiköltség-térítés illeti meg. A vizsgabizottság tagjai az útiköltség-térítésen felül tiszteletdíjban részesíthetők.

6. Ha a vizsgabizottság tagja lemond az útiköltség-térítésről, illetve a tiszteletdíjról, ennek tényét a vizsgajegyzőkönyvben rögzíteni kell.

#### II. A vizsga megkezdésének és zavartalan lefolytatásának feltételei

1. A vizsga megkezdése előtt a vizsgázó személy bemutatja személyazonosításra alkalmas fényképes okmányát. A vizsgázó kutya egyedi azonosítása chip kód leolvasásával történik.

2. A vizsgára alkalmatlan (pl. alkoholos, láthatólag bódult) állapotban megjelent vizsgázó személy a vizsgát nem kezdheti meg.

3. A vizsgára bekapcsolt mobiltelefont, magnetofont, CD-lejátszót, kép és/vagy szöveg tárolására is alkalmas eszközt, egyéb elektronikus vagy nyomtatott segédeszközt (kivéve a vizsgabizottság által ellenőrzött, a vizsgához szükséges eszközöket) bevinni és azt használni nem szabad.

4. A személyazonosítására alkalmas fényképes okmánnyal nem rendelkező, a vizsgára alkalmatlan állapotban lévő, illetve a vizsgára a 3. pont szerinti meg nem engedett segédeszközöket bevivő vagy azokat a vizsgán használó vizsgázó személyt a vizsgáról ki kell zárni.

5. A vizsga kihirdetett időpontjában meg nem jelenő vizsgázó személy vizsgáját sikertelennek kell értékelni, kivéve, ha a vizsgázó személy 3 napon belül benyújtja a vizsgabizottsághoz a távolmaradását igazoló dokumentumokat (orvosi igazolás, baleseti jegyzőkönyv stb.).

6. A távolmaradásról szóló igazolás elfogadásáról a vizsgabizottság elnöke dönt. Az igazolás elfogadása esetén a vizsgázó személy a vizsgadíj ismételten történő befizetése nélkül tehet vizsgát.

7. A vizsga ideje alatt a vizsgázó személy a vizsgahelyszínt csak kivételesen indokolt esetben (pl. egészségi ok) hagyhatja el. Ha a vizsgázó személy indokolatlanul elhagyja a vizsga helyszínét, akkor a vizsgabizottság a vizsgát megszakítja és az addigi teljesítmény alapján értékeli a vizsgázó személy teljesítményét.

8. A vizsgát csak indokolt esetben lehet megszakítani, megosztani vagy részletekben végezni, így különösen alkalmatlan időjárási viszonyok, baleset vagy egészségi ok esetén. Ilyen esetben a vizsgalapon a részeredményeket kell feltüntetni, és ha aznap nem folytatható a vizsga, vagy a vizsgabizottság összetétele változik, akkor a vizsgalapon új rovatot (oszlopot) kell kitölteni, valamint fel kell tüntetni a megszakított vizsga tényét és eredményét.

9. A vizsgáról való kizárásnak és a vizsga megszakításának tényéről, valamint körülményeiről jegyzőkönyvet kell felvenni.

#### III. Tolmács, jelnyelvi tolmács igénybevétele

1. A vizsgán a vizsgázó személy tolmács vagy jelnyelvi tolmács közreműködését kérheti.

2. A tolmács és jelnyelvi tolmács nem lehet azonos a kutyát kiképző személlyel.

3. A tolmács és jelnyelvi tolmács közreműködését a vizsgajegyzőkönyvben rögzíteni kell és a vizsga megkezdése előtt fel kell szólítani a tárgyilagos közreműködésre és a hiteles fordításra.

4. Ha a tolmács, jelnyelvi tolmács nem a szakmai szabályoknak megfelelően működik közre a vizsga lefolytatásában, a vizsgabizottság a vizsgát megszakítja, a tolmácsot, jelnyelvi tolmácsot kizárja a vizsgán történő részvételből és a vizsgázó személyt ismétlővizsgára utasítja.

#### IV. A vizsga dokumentálása

1. A kutyát kiképző szervezet a vizsgáról jegyzőkönyvet vezet.

2. A közreműködő szervezet által kiállított vizsgajegyzőkönyvben a vizsgázó személyek névsorát legkésőbb a vizsgát megelőző 5. munkanapon le kell zárni. A vizsgát a névsor lezárása nélkül megkezdeni nem szabad. A vizsgajegyzőkönyvet 10 évig kell megőrizni.

3. A vizsgajegyzőkönyv tartalmazza

  * a) a vizsga időpontját,

  * b) a vizsga helyszínét,

  * c) a vizsga fajtáját, a képesítés megnevezését, amelyre a vizsga irányul,

  * d) a vizsgatárgyak megnevezését,

  * e) a vizsgázó személyek névsorát, hozzájárulásuk esetén természetes személyazonosító adataikat,

  * f) a vizsgabizottság tagjainak nevét,

  * g) a kutyát kiképző szerv megnevezését,

  * h) a kezdő és a záróértekezleten elhangzottakat,

  * i) a tolmács, jelnyelvi tolmács közreműködésének tényét,

  * j) a vizsga megszakításának, a vizsgáról való kizárásnak a tényét,

  * k) a segítő kutyák és a vizsgázó személyek értékelését, a vizsga eredményét,

  * l) a vizsga menetét befolyásoló bármely körülményt.

4. A javító- és az ismétlővizsgáról külön jegyzőkönyvet kell felvenni.

5. A vizsgajegyzőkönyvben azokat a feladatokat, amelyek alól a vizsgázó személy felmentést kapott „(FM)” jelzéssel kell jelölni.

6. A vizsgajegyzőkönyvet a vizsgabizottság tagjai és a jegyzőkönyvvezető írják alá.

7. A kutyát kiképző szervezet a vizsga teljes - az értékelés alá tartozó - szakaszát videóra rögzíti, amely tényről tájékoztatja a vizsgázót.

8. A kutyát kiképző szervezet a vizsgát követő 15 napon belül megküldi a közreműködő szervezetnek a jegyzőkönyv másolatát elektronikus formátumban, valamint a felvételről készített vágatlan másolatot lejátszható formátumban.

9. A felvételt a kutyát kiképző szervezet és a közreműködő szervezet a segítő kutya alkalmazásból történő kivonását követő egy évig megőrzi.

10. A felvételt a vizsgabizottság tagjain kívül csak a vizsgázó személy és a közreműködő szervezet által kijelölt szakértők tekinthetik meg. Az ehhez való hozzájárulásról a vizsgázó személy a vizsgát megelőzően írásban nyilatkozik.

11. A vizsgázó személy a vizsga szabályszerű lefolytatásával kapcsolatos kifogásait a vizsgát követő 15 napon belül írásban nyújthatja be a közreműködő szervezetnek. A közreműködő szervezet - szükség szerint szakértő(k) bevonásával - közvetít a probléma megoldásában, a vizsgázó személy és a vizsgabizottság, illetve a vizsgázó személy és a kutyát kiképző szervezet közötti vita rendezésében.

12. Szakértőként olyan személy jelölhető ki, aki

  * a) a kifogással érintett vizsgán vizsgázó kutya típusának megfelelő, 3. § (1) bekezdés b) pontja szerinti szakképesítéssel rendelkezik,

  * b) a kifogással érintett vizsgán vizsgázó kutya típusának megfelelő, legalább 5 éves kiképzői és vizsgáztatási gyakorlattal rendelkezik,

  * c) a kifogással érintett vizsga szervezésében, lebonyolításában, valamint a vizsgázó kutya kiképzésében nem vett részt,

  * d) a közreműködő szervezet elnökségének nem tagja, a közreműködő szervezetben tisztséget nem visel.

13. A kijelölt szakértő díját a kifogást benyújtó fizeti meg.
