+++
title = "Kutya felvezető"
subtitle = "Rövid ismertető a terápiás kutya felvezetőröl"
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-2.jpg"
tags = ["felvezető"]
categories = ["tudástár"]
summary = "Rövid ismertető a terápiás kutya felvezetőröl"
icon = "fas fa-walking"
type = "iconized"
+++

## Terápiás kutya felvezető

A terápiás kutyák munkája során betartandó állategészségügyi normák.

#### a.) Védőoltások
A segítő kutyát évente kötelező az oltani veszettség ( lásd. 164/2008. (XII. 20.) FVM rendelet ), parvovírus, szopornyica, fertőző májgyulladás és leptospirózis ellen.
Azoknál a kutyáknál, ahol a laborvizsgálat alapján a Microsporum canis okozta fertőzöttség klinikai tünetei fennállnak, a gyógykezelés, gyógyulás majd negatív laborlelet után az oltás megelőző jelleggel félévente kötelező. Az újra munkába állt, és a féléves Microsporum canis elleni oltásban részesülő kutya rendszeres ismétlő oltását leghamarabb két egymást követő negatív vizsgálat után, az állatorvos javaslata alapján fel lehet függeszteni.
Azoknál a kutyáknál, amelyek kennel köhögés fertőzöttség tüneteit mutatják, a gyógykezelés, gyógyulás majd negatív laborlelet és újra munkába állás után az oltás megelőző jelleggel évente kötelező. A rendszeres ismétlő oltását leghamarabb két egymást követő negatív vizsgálat után, az állatorvos javaslata alapján fel lehet függeszteni.

#### b.) Belső élősködők elleni védelem
A segítő kutyánál 3 havonta kell alkalmazni féreghajtást kombinált hatóanyagú, széles hatásspektrumú anthelmintikummal (hatóanyagai lehetőség szerint praziquantel, és pirantelt (pamoát vagy embonát só), és febantel vagy fenbendazol).

#### c.) Külső élősködők elleni védelem
A terápiás kutyánál folyamatosan szükséges a külső élősködők elleni védekezés, melynek bolhára és rühatkára mindenképpen, kullancs ellen pedig ajánlottan hatékonynak kell lennie. A megelőzésre bármely hazai forgalomban lévő készítmény használható, a kezeléseket pedig a használt szer hatástartamától függő időközökben kell alkalmazni.
Terápiás foglalkozásokon részt vevő kutyáknál nem szabad bolha és/vagy kullancsnyakörvet alkalmazni, ugyanígy nem javasolt a bolha- és /vagy kullancsirtó por illetve a bolha- és /vagy kullancsirtó sampon használata. Amennyiben a fentiek használatához a gazda praktikussági vagy gazdasági okok miatt ragaszkodik, használatuk azzal a feltétellel engedhető meg, hogy azok a terápiás foglalkozást megelőző három napon belül az állattal nem kerülnek érintkezésbe. A bolha és/vagy kullancsirtó „spot on” készítmény használatát úgy kell időzíteni, hogy azok a terápiás foglalkozást megelőző három napnál korábban kerüljenek alkalmazásra.

#### d.) Szűrővizsgálatok
A terápiás kutyát félévente teljes állatorvosi fizikális vizsgálatnak kell alávetni, valamint szükséges a bélsár (széklet) parazitológiai és bakteriológiai vizsgálata különös tekintettel a szalmonellára. Az első vizsgálatot úgy kell elvégeztetni, hogy az összes eredmény megérkezésének és leigazolásának ideje legalább egy héttel előzze meg a terápiás kutya munkába állásának időpontját.
Terápiás kutyák esetében amennyiben az aktív terápiás munkában legalább egy év szünetet tartanak, úgy a vizsgálatot a fent leírtaknak megfelelően a munkakezdés előtt el kell végeztetni.
A terápiás foglalkozásokon részt vevő kutyánál a félévente kötelező állatorvosi vizsgálat során a következő szűrővizsgálatokat szükséges elvégeztetni:
– bélsár (széklet) vizsgálat – paraziták jelenléte és bakteriológiai tenyésztéses vizsgálat, különös tekintettel a szalmonellára
– szőr vizsgálat – külső élősködők, gombás fertőzöttség jelenléte
– toroktampon – bakterológiai tenyésztés ( Streptococcus, Staphylococcus, MRSA! ) fertőzésre

Fertőzöttség esetén a kutya csak a kezelés utáni negatív vizsgálati eredmény birtokában dolgozhat újra (kivétel Salmonella és MRSA, ezeknél két egymást követő negatív lelet szükséges).

A terápiás kutya diszplázia szűrése javasolt, de a munkába állásnak nem feltétele, a kutya a vizsgálat eredményétől függetlenül dolgozhat addig, amíg állapota tünetmentes. Amennyiben a kutyán a diszpláziával összefüggésbe hozható tünetek jelentkeznek, akkor állatorvosi szakvélemény kérése szükséges arról, hogy a terápiás munka közbeni fizikai megterhelés okoz-e fájdalmat a kutya számára. Az állatorvos vizsgálata, javaslata és esetleges gyógykezelési protokollja alapján a gazdának józan belátása szerint kell döntenie arról, hogy a kutya milyen programot, milyen terheléssel és feltételekkel vállalhat. Diszplázia műtéten átesett kutyák a teljes gyógyulás után dogozhatnak.

#### e.) Teendők a kutya betegsége esetén
Ha a terápiás kutya a betegség tüneteit mutatja (levertség, láz, köhögés, hányás, hasmenés), akkor nem szabad vele dogozni, csak az állatorvos által igazolt teljes gyógyulás és tünetmentesség elérését követően.
Amennyiben a terápiás kutya 3 napnál tovább tartó hasmenés, hányás, vagy hurutos köhögés tüneteit mutatja, illetve ha egy egyszerűnek látszó betegségnél az állat nem reagál a szokásos kezelésre, vagy a tünetek rövid idő alatt visszatérnek, akkor a féléves szűrés protokollja (lásd d pont) szerinti felülvizsgálat szükséges.

#### f.) Általános higiéniai szabályok
Terápiás foglalkozáson csak tiszta kutyával szabad megjelenni, célszerű a foglalkozás megkezdése előtt a kutyák szemét, szájkörnyékét, végbélnyílás környékét és a mancsokat (benti foglalkozás esetén) nedves törlőkendővel áttörölni.
