+++
title = "Kutya kiképző"
subtitle = "Rövid ismertető a kutya kiképzőröl"
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-2.jpg"
tags = ["kiképző"]
categories = ["tudástár"]
summary = "Rövid ismertető a kutya kiképzőröl"
icon = "fas fa-chalkboard-teacher"
type = "iconized"
+++

Terápiás kutyakiképző képzés
