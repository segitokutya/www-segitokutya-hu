+++
title = "Segítő kutyák"
subtitle = "Rövid ismertető a segítő kutyákról"
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-2.jpg"
tags = ["segítőkutya"]
categories = ["tudástár"]
summary = "Rövid ismertető a segítőkutyákról"
icon = "fas fa-dog"
type = "iconized"
toc = true
+++

## Mozgássérült segítő kutyák

A mozgássérült segítő kutyák olyan fajta segítséget tudnak nyújtani fogyatékkal élő embertársainknak, amelyre mi emberek nem vagyunk képesek. Rendelkezésükre állnak a nap 24 órájában, segítőkészségük kimeríthetetlen, őszinte szeretet adnak előítélet és elvárások nélkül. Segítenek fizikailag, érzelmileg, lelkileg. Támaszként szolgálnak gazdáiknak, miközben mindent megtesznek, hogy segítsék őket.

A legtöbben el sem tudjuk képzelni milyen az, ha beesik a kerekes szék alá egy tárgy, és nem tudjuk felvenni, ha a kiválasztott üzletbe, vagy gyógyszertárba lépcső vezet, és nem tudunk szólni az eladónak. Ilyenkor a kerekesszékben élőknek az az egyetlen lehetősége, hogy megkér valakit, persze ha van valaki a közelben, hogy segítsen. Kutyával mindez könnyebb, mert őt megkérni egész más, mint egy embert, a kutya lelkes, és boldoggá teszi, hogy gazdáját szolgálhatja, mindig ott van, és szívesen fogadják más emberek is, ha például őt küldi be gazdája egy üzletbe, melyet nem tud megközelíteni.

A kutyák segítenek a közlekedésben, ajtó nyitással, villanykapcsolással, kosarat visznek a szájukban, amibe pár dolgot bele lehet tenni, így a gazda keze szabadon maradhat. Segítenek öltözni, odaviszik a gazdihoz a cipőt, ruhadarabokat, segítenek a ruhákat levenni, le tudják húzni a kabát újját, a kesztyűt, zoknit. Felveszik, ami leesett, még a legapróbb dolgokat, akár egy pénzérmét is, el lehet küldeni őket tárgyakért, odaviszik a telefont, kulcsot, távirányítót, stb. Tárgyakat lehet küldeni velük más emberekhez, pl. kulcsot a kapuhoz, ha látogató érkezik.

Ha szeretne mozgássérült segítő kutya tulajdonosa lenni, vegye fel velünk a kapcsolatot.

## Rohamjelző kutyák

A rohamjelző kutyák a rohamszerű állapotoktól szenvedőknek nyújtanak felbecsülhetetlen segítséget. Különleges érzékelésüknek köszönhetően képesek a közeledő rohamot (pl. epilepsziás roham, vagy cukorbeteg hipoglikémiás sokkja) érzékelni, és ezt jelzik a gazdának. Így a gazdának lehetősége van biztonságba helyezni magát, hogy a roham alatt ne sérüljön meg, vagy gyógyszert bevenni, és így akár meg is előzheti a rohamot.

Alapítványunk a rohamjelző kutyák képzését az országban másodikként 2009-ben kezdte meg. A kiképzés során a PACSI Alapítványtól tanult Erik Kersting féle módszert alkalmazzuk, mely a kiválasztó tesztrendszernek és a speciális képzésnek köszönhetően 100%-os hatékonysággal működik. A teszteken alkalmasnak talált kutyák a kiképzés alatt valóban megtanulnak rohamot jelezni, és nincs selejtezés alkalmatlanság miatt, mert minden programba kerülő kutya működik. Ellentétben az amerikai módszerrel, ahol a képzésbe vont kutyák 50%-a sosem jelez rohamot.

A rohamjelző kutyák segítő kutya feladatokat is megtanulnak, és a rohamok jelzése mellett képesek segíteni is a roham alatt, a megtanult módon segítséget képesek hívni, jeleznek családtagnak, vagy utcán idegen embernek is, ha segítség kell. Ez azért nagy dolog, mert sokan, akiknek rohamaik vannak nem mernek kimozdulni, mert félnek, hogy elvesztik eszméletüket az utcán, és nem lesz segítség, sajnos valóban vannak rossz tapasztalatok is ez ügyben.

A kutyával rendelkező betegeknél az a tapasztalat, hogy enyhül a szorongásuk, önállóbbak, többet mozdulnak ki, magabiztosabbak, a rohamaik sűrűsége, és azok intenzitása csökken.

Ha szeretne rohamjelző kutya gazdája lenni, kérjük vegye fel velünk a kapcsolatot.

## Személyi segítő kutyák

A személyi segítő kutyák olyan felnőtteknek és gyermekeknek segítenek, akiknek fejlesztésre van szükségük valamilyen sérülés, veleszületett fogyatékosság, akadályozottság miatt.

A személyi segítők ugyanazt a feladatot végzik, mint a terápiás kutyák, de mindezt a gazdi otthonában, folyamatosan. A terápiák alatt látszik a célszemélyeken, hogy mennyivel lelkesebbek, úgy végzik a feladatokat, hogy közben észre sem veszik, hogy dolgoznak. A kutya nagyon jó motivációs eszköz. A legtöbb intézményben heti egy legfeljebb két alkalommal vannak terápiák, sajnos a lehetőségek végesek, és magasabb csoportlétszám mellett nem túl sok idő jut egy személyre. A személyi segítő kutya mindezekre képes, de mindig kéznél van, napi rendszerességgel segíti a fejlesztést, ami így látványosan felgyorsulhat. A fejlesztési programot terapeuta segítségével dolgozzuk ki, és segítünk a gazdának, hogyan kivitelezze ezeket. Mindemellett a kutya jelenléte is igazi segítség, hiszen felvidítja, mozgásra készteti gazdáját. Feltétel nélküli szeretete, és odaadása nagyon sokat jelent a sérült embereknek.

Ha Önnek vagy gyermekének segítő kutyára van szüksége vegye fel velünk a kapcsolatot.
