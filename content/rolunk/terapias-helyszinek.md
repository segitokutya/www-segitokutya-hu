+++
title = "Terápiás helyszínek"
subtitle = "Rövid összefoglaló"
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-3.jpg"
tags = ["terápia", "helyszín"]
categories = ["rólunk"]
summary = "Ezeken a helyeken részesülhet kutyás terápiában."
toc = true
+++

## Debrecen

* Faragó Utcai Óvoda

* Kenézy Gyula Kórház és Rendelőintézet-Gyermekrehabilitációs Osztály

* Kenézy Gyula Kórház és Rendelőintézet-Gyermekpszichiátria

* Debreceni Fogyatékosokat Ellátó Intézet

* Dózsa György Általános Iskola

* Talentum Baptista Általános Iskola


## Dombóvár

* „Reménység” Napközi Otthon és Fejlesztő Iskola


## Kalocsa

* Nebuló EGYMI

* Zöldfa Úti Tagóvoda

* Értelmi Fogyatékosok Napközi Otthona


## Kecskemét

* Kecskeméti EGYMI Készségfejlesztő Speciális Szakiskola és Logopédiai Intézet Autizmus Centruma


## Miskolc

* B.-A.-Z. Megyei Kórház és Egyetemi Oktató Kórház – Gyermekrehabilitációs Osztály

* Miskolci Családsegítő Központ Értelmi Fogyatékosok Napközi Otthona és Fogyatékos Személyek Gondozóháza

* Éltes Mátyás Övoda, Általános iskola és Egységes Gyógypedagógiai Módszertani Intézmény Gagarin utcai tagozata

* Miskolci Autista Alapítvány

* Gyermekek Átmeneti Otthona

* Miskolci Egyesített  Szociális  és Gyermekvédelmi Intézmény Fogyatékos Személyek Gondozóháza Erzsébet Hospice Otthon  Alapítvány


## Nyírbátor

* SZ-SZ-B. Megyei Pedagógiai Szakszolgálat Nyírbátori Tagintézménye


## Nyírbogdány

* Nyírbogdányi Református Egyházközség SION Fejlesztő Nevelést- Oktatást Végző Iskola


## Nyíregyháza

* RIDENS Szakképző Iskola, Speciális Szakiskola és Kollégium

* Halmozottan Sérültek és Szüleiknek Szolcs-Szatmár-Bereg Megyei Egyesülete


## Szeged

* SZT Bárczi Gusztáv EGYMI

* Dr. Waltner Károly Otthon

* Szegedi Zrínyi Ilona Általános Iskola

* Szegedi Madách Imre Általános Iskola

* Fogyatékkal Élő Gyermekek Nappali Intézménye és “Makray” Fejlesztő Iskola

* Szegedi Fekete István Általános Iskola
