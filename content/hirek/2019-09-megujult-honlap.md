+++
title = "Megújult a honlapunk"
summary = "Új arculat és új design"
author = "Tamás"
date = "2019-09-21"
tags = ["honlap"]
categories = ["hírek"]
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-2.jpg"
+++

## Bevezető

Idén megújúlt a honlapunk!
