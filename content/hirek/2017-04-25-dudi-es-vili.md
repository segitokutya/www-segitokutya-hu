+++
title = "Dudi és Vili"
summary = "Személyi segítő kutyánk és gazdája: Dudi és Vili"
author = "AURA"
date = "2017-04-25"
tags = ["fszk", "vizsga"]
categories = ["hírek"]
bgImage = "img/slider-bg.jpg"
image = "img/hirek/2017-04-25-dudi-es-vili1.jpg"
+++

Vili lelkesen készül a személysegítő kutya vizsgára Dudival és az anyukájával. Vili, nagyon ügyesek vagytok!

{{< figure src="/img/hirek/2017-04-25-dudi-es-vili2.jpg" alt="Dudi és Vili" width="100%" >}}

{{< figure src="/img/hirek/2017-04-25-dudi-es-vili3.jpg" alt="Dudi és Vili" width="100%" >}}

{{< figure src="/img/hirek/2017-04-25-dudi-es-vili4.jpg" alt="Dudi és Vili" width="100%" >}}
