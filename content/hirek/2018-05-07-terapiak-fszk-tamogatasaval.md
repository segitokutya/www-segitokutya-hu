+++
title = "Terápiák az FSZK támogatásával"
summary = "A terápiáink megvalósításában idén is hatalmas segítséget nyújtott az FSZK."
author = "AURA"
date = "2018-05-07"
tags = ["fszk", "terápia"]
categories = ["hírek"]
bgImage = "img/slider-bg.jpg"
image = "img/hirek/2019-05-07-terapiak-fszk-tamogatasaval.jpg"
+++

A terápiáink megvalósításában idén is hatalmas segítséget nyújtott a Fogyatékos
Személyek Esélyegyenlőségéért Közhasznú Nonprofit Kft.
