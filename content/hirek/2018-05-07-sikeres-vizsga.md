+++
title = "Sikeres vizsga"
summary = "Sikeres mozgássérült segítő kutya vizsga az FSZK támogatásával!"
author = "AURA"
date = "2018-05-07"
tags = ["fszk", "vizsga"]
categories = ["hírek"]
bgImage = "img/slider-bg.jpg"
image = "img/hirek/2018-05-07-sikeres-vizsga.jpg"
+++

Sikeres mozgássérült segítő kutya vizsga az FSZK támogatásával!

Április 29-én délelőtt duplán örülhettünk, mivel Ildikó Fügével, Juci pedig Lüszivel tett sikeres mozgássérült segítő kutya vizsgát.

A vizsgázók felkészülését a Fogyatékos Személyek Esélyegyenlőségéért Közhasznú Nonprofit Kft. támogatta, amit ezúton is szeretnénk megköszönni!
