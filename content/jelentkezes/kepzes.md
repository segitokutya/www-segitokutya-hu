+++
title = "Jelentkezés képzésre"
subtitle = "Az általunk szervezett képzésekre"
bgImage = "img/slider-bg.jpg"
image = "img/blog/blog-post-2.jpg"
tags = ["képzés", "jelentkezés"]
categories = ["jelentkezés"]
icon = "fas fa-chalkboard-teacher"
summary = "Ezeken a helyeken vehet részt az általunk szervezett képzéseken."
+++

Az AURA Segítő Kutya Alapítvány 2007-ben alakult meg. Az alapító és a
kuratórium szándéka az, hogy a kutyás terápia módszerét és a segítő kutyák
alkalmazásának előnyeit széles körben megismertesse és elterjessze.

A következő városokban, különböző helyszíneken indult kutyás terápia:

2007- 2012. június 08 –ig Debrecenben az EJK GYERMEKREHABILITÁCIÓS KÖZPONT- ban tartottunk terápiás kutyák bevonásával fejlesztő foglalkozásokat a 2-7 év közötti sajátos nevelési igényű gyerekeknek.

2009-től a debreceni Faragó Utcai Óvodában is terápiázunk kutyákkal hasonló célcsoporttal.

2009 augusztusában Debrecenben, terápiás kutya vizsgára felkészítő tábort tartottunk, olyan kutyások számára, akik szeretnének kutyájukkal vizsgázni. 15 fő, 16 kutyával vett részt a felkészítésben. Olyan jól sikerült a tábor, hogy a résztvevők közül, 7 felvezető-kutya páros önkéntesként csatlakozott alapítványunkhoz. Ők Nyíregyházán, Szegeden,  Sátoraljaújhelyen indítottak kutyás terápiát.

Nyíregyházán a RIDENS Szakképző Iskola, Speciális Szakiskola és Kollégiumban heti 2 alkalommal 5-5 fő, 14-16 év közötti enyhén értelmileg akadályozott, és/vagy mozgássérült gyerek fejlesztésébe kapcsolódott be a terápiás kutya.

Szegeden a Szegedi Kistérség Többcélú Társulása Egységes Gimnáziuma, Általános Iskolája, Óvodája, Pedagógiai Szakszolgálata Sólyom utcai Óvodájában és Általános Iskolájában 2 páros kezdte el a terápiát heti 2 alkalommal. Célcsoportjuk 3-10 év közötti fogyatékkal élő gyerekek (autista, értelmileg akadályozott, hallássérült, mozgássérült).

Sátoraljaújhelyen 1 páros indított kutyás terápiát a Deák Úti Általános Iskola, Előkészítő, Speciális Szakiskola és Egységes Pedagógiai Szakszolgálatnál heti 2 alkalommal 5-5 fő. 6-10 éves fogyatékkal élő gyerek számára.

2010-2012-ben újabb felvezető-kutya párosok csatlakoztak hozzánk.

2010-ben Nyíregyházán 1 páros, 2011-ben 2 páros Kecskeméten, 2012-ben Hódmezővásárhelyen 1 felvezető 3 kutyával csatlakozott alapítványunkhoz, és kezdte el új helyszíneken a kutyás terápiát.

2012-ben a Sátoraljaújhelyen dolgozó párosunk elköltözött Kalocsára és ott indította el a kutyás terápiát.

További eredményeink: Az AURA Segítő Kutya Alapítvány tagja a Magyar Terápiás és Segítőkutyás Szövetségnek (MATESZE). A MATESZ felkérésére 2008-2009-ben közreműködtünk a 27/2009.SZMM Rendelet megalkotásában, mely a segítő kutyák jogi státusát, alkalmazását, vizsgáztatását szabályozza. Szintén közreműködtünk 2 OKJ-s képzés szakmai és vizsgakövetelményének megalkotásában (Habilitációs kutyakiképző és Jelzőkutya kiképző).

2010-től a MATESZ – szel és a MAGYARORSZÁGI MAGISZTER ALAPÍTVÁNNYAL együttműködve indítottunk habilitációs kutyakiképző OKJ-s képzést Debrecenben.

2010-ben sikeresen pályáztunk 4 fő kutyakiképző alkalmazására az Országos Foglalkoztatási Közalapítványnál (OFA). Az OFA 2011.08.01.-TŐL 2013.07.31.-ig támogatja a 4 kutyakiképző alkalmazását. Így lehetőségünk nyílt a terápiák mellett személyi segítő kutyák kiképzésének elindítására is. Jelenleg 2 személyi segítő kutya kiképzése folyik. Egyikük egy debreceni család 2 autista gyermekét (6 és 3 évesek), a másik egy nagyváradi halmozottan sérült gyermek (5 éves) egyéni fejlesztését fogja segíteni. Jelenleg mindkét család felkészítése folyik, ismereteket kapnak a személyi segítő kutya tartásáról és kiképzéséről. Mindkét kutya vizsgája 2012 év végéig várható.
